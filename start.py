import logging
import time
import os
from logging.handlers import TimedRotatingFileHandler

os.makedirs("./logs", exist_ok=True)
logging.basicConfig(level=logging.DEBUG)
logname = "./logs/my_app.log"
handler = TimedRotatingFileHandler(logname, when="S", backupCount=5)
handler.setFormatter(logging.Formatter('TEST task 4 %(asctime)s %(levelname)s %(message)s', datefmt="%Y-%m-%d %H:%M:%S"))

logger = logging.getLogger()
logger.addHandler(handler)

for i in range(300):
    logger.info(f"{i}")
    time.sleep(2)
