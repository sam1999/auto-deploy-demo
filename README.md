### set githooks
```
vim .git/hooks/post-merge 
```

```sh
#!/bin/sh
# .git/hooks/post-merge

echo
echo "=== deploy service after updating code ==="
echo <password> | sudo -S systemctl restart auto-deploy-demo.service
echo
```


```sh
chmod +x .git/hooks/post-merge
```

### set service
```sh
git update-index --assume-unchanged *service
```

```sh
chmod +x git-pull-loop.sh
chmod +x auto-deploy-demo.service
chmod +x auto-deploy-demo-pull.service
```

```
# modify path
pwd
vim *service -p
```

```sh
sudo cp auto-deploy-demo.service /etc/systemd/system/auto-deploy-demo.service
sudo cp auto-deploy-demo-pull.service /etc/systemd/system/auto-deploy-demo-pull.service
```

```sh
sudo systemctl status auto-deploy-demo.service
sudo systemctl status auto-deploy-demo-pull.service
```

```sh
sudo systemctl start auto-deploy-demo.service
sudo systemctl start auto-deploy-demo-pull.service
```

```sh
sudo systemctl stop auto-deploy-demo.service
sudo systemctl stop auto-deploy-demo-pull.service
```

```sh
sudo systemctl disable auto-deploy-demo.service
sudo systemctl disable auto-deploy-demo-pull.service
```

### observation commands
```sh
watch -n 1 ls logs
```

```sh
watch -n 1 cat logs/my_app.log
```

```sh
watch git log --oneline
```

```sh
journalctl -xefu auto-deploy-demo.service 
```

```sh
journalctl -xefu auto-deploy-demo-pull.service 
```
